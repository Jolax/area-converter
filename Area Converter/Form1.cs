﻿using System;
using System.Windows.Forms;

namespace Area_Converter
{
    public partial class BaseForm : Form
    {

        private const double RopaniToSqMeter = 508.74;
        private const double SqMeterToSqft = 10.7639;
        private const double SqMeterToTsubo = 0.3025;
        private const double BighaToSqMeter = 6772.41;

        private double[] _finalvals;



        public BaseForm()
        {
            InitializeComponent();
        }

        private void baseform_Activated(object sender, EventArgs e)
        {
            input_1.Focus();
        }

        private void ropani_select_CheckedChanged(object sender, EventArgs e)
        {
            HandleCheckChanged();
        }

        private void bigha_select_CheckedChanged(object sender, EventArgs e)
        {
            HandleCheckChanged();
        }

        private void HandleCheckChanged()
        {
            //swap input labels
            if (ropani_select.Checked)
            {
                input_label_1.Text = @"Ropani";
                input_label_2.Text = @"Ana";
                input_label_3.Text = @"Paisa";
                input_label_4.Text = @"Dam";

            }
            if (bigha_select.Checked)
            {
                input_label_1.Text = @"Bigha";
                input_label_2.Text = @"Kattha";
                input_label_3.Text = @"Dhur";
                input_label_4.Text = @"Durkhi";
            }

            //reset output values
            _finalvals = new double[] { 0, 0, 0, 0 };
            DisplayOutput();
            ResetInputFields();
        }

        private void input_TextChanged(object sender, EventArgs e)
        {
            SetFinalValues();

            //output display
            DisplayOutput();
        }


        private void SetFinalValues()
        {

            var input1 = IsNumeric(input_1.Text) ? Convert.ToDouble(input_1.Text) : 0;
            var input2 = IsNumeric(input_2.Text) ? Convert.ToDouble(input_2.Text) : 0;
            var input3 = IsNumeric(input_3.Text) ? Convert.ToDouble(input_3.Text) : 0;
            var input4 = IsNumeric(input_4.Text) ? Convert.ToDouble(input_4.Text) : 0;

            _finalvals = new[] { input1, input2, input3, input4 };

        }


        private void DisplayOutput()
        {

            m_output.Text = $@"{GetSqMeter():n}";
            f_output.Text = $@"{GetSqFeet():n}";
            t_output.Text = $@"{GetTsubo():n}";

        }

        private double GetSqMeter()
        {
            double totalSqmeter = 0;
            if (ropani_select.Checked)
            {
                totalSqmeter = _finalvals[0] * RopaniToSqMeter + (_finalvals[1] / 16 * RopaniToSqMeter) + (_finalvals[2] / 64 * RopaniToSqMeter) + (_finalvals[3] / (64 * 4) * RopaniToSqMeter);
            }
            else if (bigha_select.Checked)
            {
                totalSqmeter = _finalvals[0] * BighaToSqMeter + (_finalvals[1] / 20 * BighaToSqMeter) + (_finalvals[2] / 400 * RopaniToSqMeter) + (_finalvals[3] / (400 * 20) * RopaniToSqMeter);
            }

            return totalSqmeter;
        }

        private double GetSqFeet()
        {
            return Math.Round(GetSqMeter() * SqMeterToSqft, 2);
        }

        private double GetTsubo()
        {
            return Math.Round(GetSqMeter() * SqMeterToTsubo, 2);
        }

        private void ResetInputFields()
        {
            input_1.Text = "";
            input_2.Text = "";
            input_3.Text = "";
            input_4.Text = "";
        }

        private void input_1_Keydown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter && e.KeyCode != Keys.Right && e.KeyCode != Keys.Space) return;
            input_2.Focus();
            e.SuppressKeyPress = true;
        }


        private void input_2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Right || e.KeyCode == Keys.Space)
            {
                input_3.Focus();
                e.SuppressKeyPress = true;
            }

            if ((e.KeyCode == Keys.Back || e.KeyCode == Keys.Left) && input_2.TextLength == 0)
            {
                input_1.Focus();
            }
        }

        private void input_3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Right || e.KeyCode == Keys.Space)
            {
                input_4.Focus();
                e.SuppressKeyPress = true;
            }
            if ((e.KeyCode == Keys.Back || e.KeyCode == Keys.Left) && input_3.Text.Length == 0)
            {
                input_2.Focus();
            }
        }

        private void input_4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Right || e.KeyCode == Keys.Space)
            {
                e.SuppressKeyPress = true;
            }
            if ((e.KeyCode == Keys.Back || e.KeyCode == Keys.Left) && input_4.Text.Length == 0)
            {
                input_3.Focus();
            }

        }
        private static bool IsNumeric(string value)
        {
            var isNum = double.TryParse(value, System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out _);
            return isNum;
        }

        private void clear_button_Click(object sender, EventArgs e)
        {
            //clear the output
            _finalvals = new double[] { 0, 0, 0, 0 };
            DisplayOutput();

            //reset input fields
            ResetInputFields();

            //focus 1st input fields
            input_1.Focus();

        }
    }
}
