namespace Area_Converter
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources =
                new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.ropani_select = new System.Windows.Forms.RadioButton();
            this.bigha_select = new System.Windows.Forms.RadioButton();
            this.input_1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.m_output = new System.Windows.Forms.Label();
            this.f_output = new System.Windows.Forms.Label();
            this.t_output = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.input_2 = new System.Windows.Forms.TextBox();
            this.input_3 = new System.Windows.Forms.TextBox();
            this.input_4 = new System.Windows.Forms.TextBox();
            this.heading_label = new System.Windows.Forms.Label();
            this.input_label_1 = new System.Windows.Forms.Label();
            this.input_label_2 = new System.Windows.Forms.Label();
            this.input_label_3 = new System.Windows.Forms.Label();
            this.input_label_4 = new System.Windows.Forms.Label();
            this.clear_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ropani_select
            // 
            this.ropani_select.AutoSize = true;
            this.ropani_select.Checked = true;
            this.ropani_select.Location = new System.Drawing.Point(18, 66);
            this.ropani_select.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.ropani_select.Name = "ropani_select";
            this.ropani_select.Size = new System.Drawing.Size(77, 24);
            this.ropani_select.TabIndex = 1;
            this.ropani_select.TabStop = true;
            this.ropani_select.Text = "Ropani";
            this.ropani_select.UseVisualStyleBackColor = true;
            this.ropani_select.CheckedChanged += new System.EventHandler(this.ropani_select_CheckedChanged);
            // 
            // bigha_select
            // 
            this.bigha_select.AutoSize = true;
            this.bigha_select.Location = new System.Drawing.Point(101, 66);
            this.bigha_select.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bigha_select.Name = "bigha_select";
            this.bigha_select.Size = new System.Drawing.Size(68, 24);
            this.bigha_select.TabIndex = 2;
            this.bigha_select.Text = "Bigha";
            this.bigha_select.UseVisualStyleBackColor = true;
            this.bigha_select.CheckedChanged += new System.EventHandler(this.bigha_select_CheckedChanged);
            // 
            // input_1
            // 
            this.input_1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.input_1.Location = new System.Drawing.Point(17, 116);
            this.input_1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.input_1.Name = "input_1";
            this.input_1.Size = new System.Drawing.Size(73, 27);
            this.input_1.TabIndex = 3;
            this.input_1.TextChanged += new System.EventHandler(this.input_TextChanged);
            this.input_1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.input_1_Keydown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.label1.Location = new System.Drawing.Point(15, 196);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "M²";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(155, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Ft²";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(275, 196);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Tsubo";
            // 
            // m_output
            // 
            this.m_output.AutoSize = true;
            this.m_output.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.m_output.Location = new System.Drawing.Point(15, 222);
            this.m_output.Name = "m_output";
            this.m_output.Size = new System.Drawing.Size(17, 17);
            this.m_output.TabIndex = 8;
            this.m_output.Text = "0";
            this.m_output.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // f_output
            // 
            this.f_output.AutoSize = true;
            this.f_output.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.f_output.Location = new System.Drawing.Point(155, 222);
            this.f_output.Name = "f_output";
            this.f_output.Size = new System.Drawing.Size(17, 17);
            this.f_output.TabIndex = 9;
            this.f_output.Text = "0";
            this.f_output.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // t_output
            // 
            this.t_output.AutoSize = true;
            this.t_output.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold,
                System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.t_output.Location = new System.Drawing.Point(275, 222);
            this.t_output.Name = "t_output";
            this.t_output.Size = new System.Drawing.Size(17, 17);
            this.t_output.TabIndex = 10;
            this.t_output.Text = "0";
            this.t_output.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(211, 266);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 40);
            this.label4.TabIndex = 12;
            this.label4.Text = "®Jolax Transys Pvt. Ltd.\r\ncontact@jolax-transys.com";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // input_2
            // 
            this.input_2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.input_2.Location = new System.Drawing.Point(108, 116);
            this.input_2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.input_2.Name = "input_2";
            this.input_2.Size = new System.Drawing.Size(73, 27);
            this.input_2.TabIndex = 13;
            this.input_2.TextChanged += new System.EventHandler(this.input_TextChanged);
            this.input_2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.input_2_KeyDown);
            // 
            // input_3
            // 
            this.input_3.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.input_3.Location = new System.Drawing.Point(205, 116);
            this.input_3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.input_3.Name = "input_3";
            this.input_3.Size = new System.Drawing.Size(73, 27);
            this.input_3.TabIndex = 14;
            this.input_3.TextChanged += new System.EventHandler(this.input_TextChanged);
            this.input_3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.input_3_KeyDown);
            // 
            // input_4
            // 
            this.input_4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.input_4.Location = new System.Drawing.Point(301, 116);
            this.input_4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.input_4.Name = "input_4";
            this.input_4.Size = new System.Drawing.Size(73, 27);
            this.input_4.TabIndex = 15;
            this.input_4.TextChanged += new System.EventHandler(this.input_TextChanged);
            this.input_4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.input_4_KeyDown);
            // 
            // heading_label
            // 
            this.heading_label.AutoSize = true;
            this.heading_label.BackColor = System.Drawing.SystemColors.Menu;
            this.heading_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F,
                System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
            this.heading_label.Location = new System.Drawing.Point(10, 12);
            this.heading_label.Name = "heading_label";
            this.heading_label.Size = new System.Drawing.Size(159, 25);
            this.heading_label.TabIndex = 0;
            this.heading_label.Text = "Area Converter";
            // 
            // input_label_1
            // 
            this.input_label_1.AutoSize = true;
            this.input_label_1.Location = new System.Drawing.Point(14, 148);
            this.input_label_1.Name = "input_label_1";
            this.input_label_1.Size = new System.Drawing.Size(56, 20);
            this.input_label_1.TabIndex = 16;
            this.input_label_1.Text = "Ropani";
            // 
            // input_label_2
            // 
            this.input_label_2.AutoSize = true;
            this.input_label_2.Location = new System.Drawing.Point(106, 148);
            this.input_label_2.Name = "input_label_2";
            this.input_label_2.Size = new System.Drawing.Size(35, 20);
            this.input_label_2.TabIndex = 17;
            this.input_label_2.Text = "Ana";
            // 
            // input_label_3
            // 
            this.input_label_3.AutoSize = true;
            this.input_label_3.Location = new System.Drawing.Point(203, 148);
            this.input_label_3.Name = "input_label_3";
            this.input_label_3.Size = new System.Drawing.Size(42, 20);
            this.input_label_3.TabIndex = 18;
            this.input_label_3.Text = "Paisa";
            // 
            // input_label_4
            // 
            this.input_label_4.AutoSize = true;
            this.input_label_4.Location = new System.Drawing.Point(298, 148);
            this.input_label_4.Name = "input_label_4";
            this.input_label_4.Size = new System.Drawing.Size(41, 20);
            this.input_label_4.TabIndex = 19;
            this.input_label_4.Text = "Dam";
            // 
            // clear_button
            // 
            this.clear_button.Location = new System.Drawing.Point(301, 68);
            this.clear_button.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.clear_button.Name = "clear_button";
            this.clear_button.Size = new System.Drawing.Size(75, 29);
            this.clear_button.TabIndex = 20;
            this.clear_button.Text = "Clear";
            this.clear_button.UseVisualStyleBackColor = true;
            this.clear_button.Click += new System.EventHandler(this.clear_button_Click);
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(388, 311);
            this.Controls.Add(this.clear_button);
            this.Controls.Add(this.input_4);
            this.Controls.Add(this.input_3);
            this.Controls.Add(this.input_2);
            this.Controls.Add(this.input_1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.t_output);
            this.Controls.Add(this.f_output);
            this.Controls.Add(this.m_output);
            this.Controls.Add(this.bigha_select);
            this.Controls.Add(this.ropani_select);
            this.Controls.Add(this.heading_label);
            this.Controls.Add(this.input_label_4);
            this.Controls.Add(this.input_label_3);
            this.Controls.Add(this.input_label_2);
            this.Controls.Add(this.input_label_1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon) (resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BaseForm";
            this.Padding = new System.Windows.Forms.Padding(10, 12, 10, 12);
            this.ShowInTaskbar = true;
            this.Text = "Area Converter";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int) (((byte) (224)))), ((int) (((byte) (224)))),
                ((int) (((byte) (224)))));
            this.Activated += new System.EventHandler(this.baseform_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion
        private System.Windows.Forms.RadioButton ropani_select;
        private System.Windows.Forms.RadioButton bigha_select;
        private System.Windows.Forms.TextBox input_1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label m_output;
        private System.Windows.Forms.Label f_output;
        private System.Windows.Forms.Label t_output;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox input_2;
        private System.Windows.Forms.TextBox input_3;
        private System.Windows.Forms.TextBox input_4;
        private System.Windows.Forms.Label heading_label;
        private System.Windows.Forms.Label input_label_1;
        private System.Windows.Forms.Label input_label_2;
        private System.Windows.Forms.Label input_label_3;
        private System.Windows.Forms.Label input_label_4;
        private System.Windows.Forms.Button clear_button;
    }
}
